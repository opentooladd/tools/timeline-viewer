import { describe, it, expect } from "vitest";

import { mount } from "@vue/test-utils";
import CsvImporter from "../CsvImporter.vue";

describe("CsvImporter", () => {
  it("renders properly", () => {
    const wrapper = mount(CsvImporter);
    expect(wrapper.html()).toContain("Déposer un fichier csv");
  });
});
