<script setup lang="ts">
import { ref, computed, watch } from "vue";
import {
  VueCsvToggleHeaders,
  VueCsvInput,
  VueCsvMap,
  VueCsvErrors,
  VueCsvImport,
} from "vue-csv-import";

interface Props {
  localFormat?: string;
}

interface Emits {
  (e: "data", data: CsvData): void;
}

const emit = defineEmits<Emits>();

const props = withDefaults(defineProps<Props>(), {
  localFormat: "PPPP",
});

const csv = ref<any[]>([]);

const columns = computed(() => {
  const colObject: { [key: string]: string } = csv.value.slice(0, 1)[0];
  if (!colObject) {
    return [];
  }
  return Object.values(colObject).map((v: string) => v.toLowerCase());
});

watch(csv, async (nValue: { [key: string]: string }[]) => {
  const allLinesExceptColumns = nValue.slice(1);
  const linesWithData = allLinesExceptColumns.filter((value) => {
    return Object.values(value)
      .slice(1)
      .some((v: string) => v !== "");
  });
  const d: { [key: string]: string }[] = linesWithData.map(
    (v: { [key: string]: string | Date }) => {
      v.dates = new Date(v.dates);
      const keys = Object.keys(v);
      const newValue = keys.reduce(
        (acc: { [key: string]: string }, k: string) => {
          acc[k.toLowerCase()] = v[k] as string;
          return acc;
        },
        {}
      );
      return newValue;
    }
  );
  emit("data", {
    columns: columns.value,
    data: d,
  });
});
</script>

<template>
  <div class="csv-importer">
    <div class="csv-importer__wrapper">
      <label class="csv-importer__label">Déposer un fichier csv</label>
      <vue-csv-import
        v-model="csv"
        :fields="{
          dates: { required: true, label: 'Dates' },
          gouvernance: { required: true, label: 'Gouvernance' },
          'montage juridique': { required: true, label: 'Montage Juridique' },
          collectif: { required: true, label: 'Collectif' },
        }"
      >
        <vue-csv-toggle-headers></vue-csv-toggle-headers>
        <vue-csv-errors></vue-csv-errors>
        <vue-csv-input></vue-csv-input>
        <vue-csv-map :auto-match="true"></vue-csv-map>
      </vue-csv-import>
    </div>
  </div>
</template>

<style lang="scss">
.csv-importer {

  label:not(.csv-importer__label) {
    display: none;
  }

  .csv-importer__wrapper {
    display: flex;
    width: 450px;
    height: 300px;
    flex-direction: column;
    justify-content: space-evenly;
    align-items: center;
    border: solid 2px var(--dark-grey);
    border-radius: 0.75rem;
    padding: 0.75rem;
  }

  .csv-importer__wrapper input {
    position: relative;
    width: 100%;
    height: 100%;
  }
}
</style>
