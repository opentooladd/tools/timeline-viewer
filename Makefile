all: install build

install:
	npm install

build:
	npm run build

dev:
	npm run dev

test-unit:
	npm run test:unit

test-e2e:
	npm run test:e2e

test-e2e-ci:
	npm run test:e2e:ci

test: test-unit test-e2e-ci

test-ci: test-unit test-e2e-ci