export interface CsvData {
  columns: string[];
  data: { dates: Date; [key: string]: string }[];
}
